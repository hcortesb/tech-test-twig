const args = process.argv.slice(2)

const groupArrayElements = (array, slices) => {
    const arrays = []

    if (Array.isArray(array) && Number.isInteger(slices) && slices>0) { //Check that the first argument is an array and the second is a positive integer
        const position = Math.ceil(array.length / slices) //The position needed to partition the array
        while (array.length > 0)
            arrays.push(array.splice(0, position)) //cuts the original array in the number of slices we defined
        return arrays
    } else {
        return 'Error: incorrect arguments'
    }
}
const arr = args[0].split(",").map(x=>{return parseInt(x, 10); }) //get the values and convert them into an array of integers
const slices = parseInt(args[1]) //parse into int the number of slices we want to cut
console.log(groupArrayElements(arr, slices))