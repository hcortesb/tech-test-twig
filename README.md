# Technical Test

This is technical test 

## Usage

Use the script at the command line to execute it.

```bash
node script.js '1,2,3,4,5' 3
```

The first parameter is the array of numbers.

The second parameter is the number of parts to be split.

## License
[MIT](https://choosealicense.com/licenses/mit/)